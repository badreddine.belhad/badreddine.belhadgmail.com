from film import *
import csv
import random
import time

filename = "DataBase/movies.csv"
Global_film = 0


def starting_function(Liste_film):

    alea_number = [0,0,0]
    n1,n2,n3 = 0,0,random.randint(0, len(Liste_film))

    while n1 == n2 or n1 == n3 or n2 == n3:
        n1 = random.randint(0, len(Liste_film))
        n2 = random.randint(0, len(Liste_film))
        n3 = random.randint(0, len(Liste_film))

    print("Nous vous proposons ces trois films :\n")
    time.sleep(2)
    print("Proposition numéro 1")
    Liste_film[n1].get_infoFilm()
    time.sleep(2)
    print("Proposition numéro 2")
    Liste_film[n2].get_infoFilm()
    time.sleep(2)
    print("Proposition numéro 3")
    Liste_film[n3].get_infoFilm()

    return n1,n2,n3


#Film,Genre,Lead Studio,Audience score %,Profitability,Rotten Tomatoes %,Worldwide Gross,Year
#self,Key,titre,genre,audS,year,Lead


def readCSV():
    file = open(filename)
    csvreader = csv.reader(file)
    header = []
    header = next(csvreader)
    rows = []
    for row in csvreader:
        film1 = film(Global_film+1,row[0],row[1],row[3],row[7],row[2])
        rows.append(film1)
    file.close()
    return rows

def reco_enfct_genre(L):
    genre = input("Rappel :\n"
                  "Pour notre démonstration, nous ne prenons en compte que les genres de film suivant : Drama, Comedy, Fantasy, Romance")
    choosenF = []
    for i in range(0,len(L)):

        if L[i].get_genre().lower() == genre.lower() :
            choosenF.append(L[i])
            #print(".........", i)

    i = "o"
    n = 0
    while i=="o" and n<len(choosenF):
        print("Nous vous proposons ce film : ")
        choosenF[n].get_infoFilm()
        i = input("Veuillez-vous une autre proposition ? O/N")
        n= n+1
    r = input("Une autre recommandation de film en fonction du genre ? N/O")
    if r =="o":
        reco_enfct_genre(L)



def trois_reco(L):
    i = 1
    while i == 1:

        n1, n2, n3 = starting_function(L)

        rep = input("Plus d'information sur un film ? O/N")
        if rep.lower() == "o":
            film_number = input("Pour plus d'information sur un film entrer son numéro")
            if film_number == "1":
                L[n1].moreInfo()
            if film_number == "2":
                L[n2].moreInfo()
            if film_number == "3":
                L[n3].moreInfo()

        repp = input("Veuillez-vous une autre proposition ? O/N")
        if repp.lower() == "n":
            i = 0

def center_page():

    print(
          "Notre système de recommandation permet 2 type de recommandations\n",
          "1. Recommandation de trois films d'actualité\n",
          "2. Recommandation en fonction du genre\n",
          "3. Pour quitter notre système de recommandation")
    return input("Pour la suite de la démonstration, veuillez choisir quel mode de recommandation vouliez-vous ?\n")

def tri():
    print("1. Recommandation de trois films d'actualité\n",
          "2. Recommandation en fonction du genre\n",
          "3. Pour quitter notre système de recommandation")
    return input("Que choisissez-vous ?")

if __name__ == '__main__':
    Liste_Film_BD = readCSV()
    print("Nous vous remercions pour la participation à cette démonstration\n")


    l = center_page()

    while int(l) != 3 :
        if int(l) == 1 :
            trois_reco(Liste_Film_BD)

        if int(l) == 2:
            reco_enfct_genre(Liste_Film_BD)

        l = tri()



            #starting_function(Liste_alea)