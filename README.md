# Badreddine.Belhadgmail.Com

## Project description

This project aims to develop a recommendation system based on the machine learning model "Cosine Simularity". 

For the training of the model, I based myself on the database recovered from Kaggle.com.  (The database is open source)

The model is based on the vectorization of the data retrieved from the movies in the database (original title, description, genre, imdb rating...). 

#Project steps:

Data mining:
- Collection and storage of data  
- Exploration of data : Column types

Data preparation:
- Text cleaning: (remove foreign languages, transform to lower(), remove stop words, remove special characters, remove punctuation, truncate, lemmatize) ( applied to original title, genre and overview)
- deletion of empty cells
- Transform date
- delete the output date

Preprocessing of data :
- Apply standard caller to numeric columns
- Concatenate all columns into one column
- Pass the concatenated column into a vectorization template -> transfer the resulting matrix into a data frame.
- save the vectorization model


## User experience

To use our recommendation system, the user enters the title of a movie they have already seen and enjoyed. Our recommendation system uses what the user has filled in to suggest movies they might like. 
If the title is not in the database used to train the model, we ask for more information about the movie, such as synopsis and genre. The system uses this information to provide an appropriate recommendation. 


## Project status

In order to realize this demonstration, we omitted the data science part.
